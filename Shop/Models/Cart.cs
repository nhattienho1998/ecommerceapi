﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class Cart : BaseModel
    {
        public int CartId { get; set; }
        public string TokenCart { get; set; }
        public decimal Amount { get; set; }
        public ICollection<CartDetail> CartDetails { get; set; }
    }
}
