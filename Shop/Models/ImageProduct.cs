﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class ImageProduct
    {
        public long ImageId { get; set; }
        public string Url { get; set; }
        public DateTime DateAdded { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
