﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class Product : BaseModel
    {
        public int ProductId { get; set; }
        public int ParentId { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
        public int? CategoryId { get; set; }
        public string Poster { get; set; }
        public Category Category { get; set; }
        public string Description { get; set; }
        public string SKU { get; set; }
        public int? BrandId { get; set; }
        public Brand Brand { get; set; }
        public string Unit { get; set; }
        public string Origin { get; set; }
        public string ShortDescription { get; set; }
        public string Link { get; set; }
        public bool IsActive { get; set; }
        public bool IsTrending { get; set; }
        public Stock Stock { get; set; }
        public ICollection<ImageProduct> ImageProducts { get; set; }
        public ICollection<AttributeProductValue> AttributeProductValues { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        public ICollection<DiscountProduct> DiscountProducts { get; set; }
        public ICollection<CartDetail> CartDetails { get; }
    }
}
