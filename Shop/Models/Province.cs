﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class Province
    {
        public string ProvinceId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public ICollection<District> Districts { get; set; }
    }
}
