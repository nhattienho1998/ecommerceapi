﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class OrderDetail : BaseModel
    {
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int? DiscountProductId { get; set; }
        public DiscountProduct DiscountProduct { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
    }
}
