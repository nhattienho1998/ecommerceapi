﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class AttributeProductValue : BaseModel
    {
        public int AttibuteValueId { get; set; }
        public string Value { get; set; }
        public int AttributeId { get; set; }
        public AttributeProduct AttributeProduct { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
