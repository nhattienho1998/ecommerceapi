﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class AttributeProduct : BaseModel
    {
        public int AttributeId { get; set; }
        public string Name { get; set; }
        public ICollection<AttributeProductValue> AttributeProductValues { get; set; }
        public ICollection<AttributeValue> AttributeValues { get; }
    }
}
