﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class DiscountCode : BaseModel
    {
        public int DiscountCodeId { get; set; }
        public string Code { get; set; }
        public decimal Value { get; set; }
        public int QuantityOrder { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
