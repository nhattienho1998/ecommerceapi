﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class Stock : BaseModel
    {
        public int StockId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public long Quantity { get; set; }
    }
}
