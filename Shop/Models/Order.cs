﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class Order :BaseModel
    {
        public int OrderId { get; set; }
        public string CustomerName { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string Address { get; set; }
        public decimal Amount { get; set; }
        public decimal ShipFee { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime OrderDate { get; set; }
        public int? DiscountCodeId { get; set; }
        public DiscountCode DiscountCode { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
