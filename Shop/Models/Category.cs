﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class Category : BaseModel
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Poster { get; set; }
        public int? ParentId { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
