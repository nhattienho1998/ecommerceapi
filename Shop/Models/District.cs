﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class District
    {
        public string DistrictId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ProvinceId { get; set; }
        public Province Province { get; set; }

        public ICollection<Ward> Wards { get; set; }
    }
}
