﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class Brand:BaseModel
    {
        public int BrandId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
