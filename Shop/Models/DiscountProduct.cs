﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class DiscountProduct : BaseModel
    {
        public int DiscountProductId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        [Range(0.01,100.00)]
        public float Value { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
