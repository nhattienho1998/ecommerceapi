﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class AttributeValue : BaseModel
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public AttributeProduct AttributeProduct { get; set; }
        public string Value { get; set; }
    }
}
