﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shop.Data.Services_Brand;
using Shop.ViewModels.Brand;

namespace Shop.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BrandController : ControllerBase
    {
        private readonly IServiceBrand _service;
        public BrandController(IServiceBrand service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpPost]
        public async Task<IActionResult> Create(BrandForCreate brand)
        {
            try
            {
                var validation = _service.ValidationBeforeCreate(brand);
                if (validation.IsValid)
                {
                    var result = await _service.Create(brand);
                    return StatusCode(201, new
                    {
                        Message = "Success!",
                        Data = result
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = validation.Errors.Values
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var result = await _service.GetAll();
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }
    }
}