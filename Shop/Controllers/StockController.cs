﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shop.Data.Services_Stock;
using Shop.Helpers.Params;
using Shop.ViewModels.Stock;

namespace Shop.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StockController : ControllerBase
    {
        private readonly IServiceStock _service;
        public StockController(IServiceStock service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpPost]
        public async Task<IActionResult> GetAll(StockParams stockParams)
        {
            try
            {
                var result = await _service.GetAll(stockParams);
                return StatusCode(500, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, StockForUpdate stock)
        {
            try
            {
                var result = await _service.UpdateStock(id, stock);
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }
    }
}