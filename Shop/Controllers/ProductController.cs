﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shop.Data.Services_Product;
using Shop.Helpers.Params;
using Shop.ViewModels.Product;

namespace Shop.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IServiceProduct _service;
        public ProductController(IServiceProduct service)
        {
            _service = service;
        }
        [Authorize(Roles = "Admin,Staff")]
        [HttpPost]
        public async Task<IActionResult> CreateParentProduct(ProductParentForCreate product)
        {
            try
            {
                var validation = _service.ValidationBeforeCreateProduct(product);
                if (validation.IsValid)
                {
                    var result = await _service.CreateParentProduct(product);
                    return StatusCode(201, new
                    {
                        Message = "Success",
                        Data = result
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = validation.Errors.Values
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = "Fail"
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpPost]
        public async Task<IActionResult> CreateChildProduct(ProductChildForCreate product)
        {
            try
            {
                var validation = _service.ValidationBeforeCreateChildProduct(product);
                if (validation.IsValid)
                {
                    var result = await _service.CreateChildProduct(product);
                    if(result == null)
                    {
                        return StatusCode(500, new
                        {
                            Message = "Fail"
                        });
                    }
                    return StatusCode(201, new
                    {
                        Message = "Success",
                        Data = result
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = validation.Errors.Values
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> GetProducts(ProductParams productParams)
        {
            try
            {
                var result = await _service.GetProductsForClient(productParams);
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpPost]
        public async Task<IActionResult> GetProductsForAdmin(ProductParams productParams)
        {
            try
            {
                var result = await _service.GetProductsForAdmin(productParams);
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetailProduct(int id)
        {
            try
            {
                var result = await _service.GetDetailProductForClient(id);
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }
        [Authorize(Roles = "Admin,Staff")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetailProductForAdmin(int id)
        {
            try
            {
                var result = await _service.GetDetailProductForAdmin(id);
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Trending()
        {
            try
            {
                var result = await _service.GetTrending();
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }
    }
}