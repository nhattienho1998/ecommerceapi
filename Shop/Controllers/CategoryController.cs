﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shop.Data.Services_Category;
using Shop.ViewModels.Category;

namespace Shop.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IServiceCategory _service;
        public CategoryController(IServiceCategory service)
        {
            _service = service;
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpPost]
        public async Task<IActionResult> Create(CategoryForCreate category)
        {
            try
            {
                var validation = _service.ValidationBeforeCreate(category);
                if (validation.IsValid)
                {
                    var result = await _service.Create(category);
                    return StatusCode(201, new
                    {
                        Message = "Success!",
                        Data = result
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = validation.Errors.Values
                    });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetTreeCategory()
        {
            try
            {
                var result = await _service.GetTreeCategory();
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var result = await _service.GetAll();
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpGet]
        public async Task<IActionResult> GetParentCategory()
        {
            try
            {
                var result = await _service.GetParentCategory();
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpGet]
        public async Task<IActionResult> GetAllChild()
        {
            try
            {
                var result = await _service.GetAllChild();
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, CategoryForUpdate category)
        {
            try
            {
                var validation = _service.ValidationBeforeUpdate(id,category);
                if (validation.IsValid)
                {
                    var result = await _service.Update(id,category);
                    return StatusCode(201, new
                    {
                        Message = "Success!",
                        Data = result
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = validation.Errors.Values
                    });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }
    }
}