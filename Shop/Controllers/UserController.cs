﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shop.Authentication;
using Shop.Authentication.FaceBook;
using Shop.Authentication.TokenManager;
using Shop.Data.Services_User;
using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.User;

namespace Shop.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IServiceUser _service;
        public UserController(IServiceUser service)
        {
            _service = service;

        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> SignUp(UserForCreate user)
        {
            try
            {
                var validation = _service.ValidationBeforeCreate(user);
                if (validation.IsValid)
                {
                    var result = await _service.SignUp(user);
                    return StatusCode(201, new
                    {
                        Message = "Success",
                        Data = new
                        {
                            userId = result.UserId,
                            userName = result.Email,
                            role = result.Role
                        }
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = validation.Errors.Values
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = "Fail"
                });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> LoginWithFacebook(FacebookLoginResource facebookLoginResource)
        {
            try
            {
                var result = await _service.LoginWithFacebook(facebookLoginResource);
                var token = await TokenProvider.GenerateToken(result);
                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = new
                    {
                        userId = result.UserId,
                        email = result.Email,
                        role = result.Role,
                        name = result.Name
                    },
                    Token = token
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = "Fail"
                });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(UserForLogin user)
        {
            try
            {
                var result = await _service.Login(user);
                if (result == null)
                {
                    return StatusCode(401, new
                    {
                        Message = "User name or passowd is incorrect!"
                    });
                }
                else
                {
                    var token = await TokenProvider.GenerateToken(result);
                    return StatusCode(200, new
                    {
                        Message = "Success",
                        UserInfo = new
                        {
                            userId = result.UserId,
                            role = result.Role,
                            email = result.Email,
                            name = result.Name
                        },
                        Token = token
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = "Login fail"
                });
            }

        }

        [Authorize(Roles = "Customer")]
        [HttpGet]
        public async Task<IActionResult> GetUserInfo()
        {
            try
            {
                var identity = this.User.Identity as ClaimsIdentity;
                var userId = identity.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                var result = await _service.GetUserInfo(int.Parse(userId));


                return StatusCode(200, new
                {
                    Message = "Success!",
                    Data = new
                    {
                        userId = result.UserId,
                        role = result.Role,
                        email = result.Email,
                        name = result.Name
                    }
                });
            }
            catch (Exception ex)
            {
                return StatusCode(401, new
                {
                    Message = "Not found!"
                });
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> CreateStaff(UserForCreateStaff staff)
        {
            try
            {
                var validation = _service.ValidationBeforeCreateStaff(staff);
                if (validation.IsValid)
                {
                    var result = await _service.CreateStaff(staff);
                    return StatusCode(201, new
                    {
                        Message = "Success",
                        Data = new
                        {
                            userId = result.UserId,
                            role = result.Role,
                            email = result.Email,
                            name = result.Name
                        }
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = validation.Errors.Values
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = "Fail"
                });
            }
        }

    }
}