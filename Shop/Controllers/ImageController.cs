﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shop.Data.Services_Image;
using Shop.ViewModels.ImageProduct;

namespace Shop.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly IServiceImage _service;
        public ImageController(IServiceImage service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> AddImage(ImageProductForCreate image)
        {
            try
            {
                var result = await _service.AddImage(image);
                return StatusCode(201, new
                {
                    Message = "Success",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddMultipleImage(ICollection<ImageProductForCreate> listImages)
        {
            try
            {
                var result = await _service.AddMultipleImage(listImages);
                if (result == null)
                {
                    return StatusCode(500, new
                    {
                        Message = "Fail"
                    });
                }
                else
                {
                    return StatusCode(201, new
                    {
                        Message = "Success",
                        Data = result
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }
    }
}