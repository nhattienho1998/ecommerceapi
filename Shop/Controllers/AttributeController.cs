﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shop.Data.Services_Attribute;
using Shop.ViewModels.AttributeProduct;

namespace Shop.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AttributeController : ControllerBase
    {
        private readonly IServiceAttributeProduct _service;
        public AttributeController(IServiceAttributeProduct service)
        {
            _service = service;
        }
        [Authorize(Roles = "Admin,Staff")]
        [HttpPost]
        public async Task<IActionResult> Create(AttributeProductForCreate attribute)
        {
            try
            {
                var validation = _service.ValidationBeforeCreate(attribute);
                if (validation.IsValid)
                {
                    var result = await _service.Create(attribute);
                    return StatusCode(201, new
                    {
                        Message = "Success",
                        Data = new
                        {
                            AttributeId = result.AttributeId,
                            Name = result.Name,
                            Status = result.Status,
                            CreateDate = result.CreateDate,
                            UpdateDate = result.UpdateDate
                        }
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = validation.Errors.Values
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = "Fail"
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var result = await _service.GetAll();
                return StatusCode(200, new
                {
                    Message = "Success",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = "Fail"
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpPost("{id}")]
        public async Task<IActionResult> AddValue(int id, ValueForCreate value)
        {
            try
            {
                var result = await _service.AddValue(id, value);
                if (result != null)
                {
                    return StatusCode(201, new
                    {
                        Message = "Success",
                        Data = result
                    });
                }
                else
                {
                    return StatusCode(500, new
                    {
                        Message = "Fail"
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = "Fail"
                });
            }
        }

        [Authorize(Roles = "Admin,Staff")]
        [HttpGet]
        public async Task<IActionResult> GetValues(int id)
        {
            try
            {
                var result = await _service.GetValues(id);
                return StatusCode(201, new
                {
                    Message = "Success",
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    Message = ex
                });
            }
        }
    }
}