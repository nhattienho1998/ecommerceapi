﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels
{
    public class ValidationResultViewModel
    {
        public bool IsValid { get; set; }
        public IDictionary<string, string> Errors { get; set; }
    }
}
