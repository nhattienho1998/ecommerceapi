﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.AttributeValue
{
    public class AttributeValueForCreate
    {
        public int AttributeId { get; set; }
        public string Value { get; set; }
    }
}
