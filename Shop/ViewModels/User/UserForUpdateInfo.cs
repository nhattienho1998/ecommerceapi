﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.User
{
    public class UserForUpdateInfo : BaseViewModel
    {
        public string Name { get; set; }
    }
}
