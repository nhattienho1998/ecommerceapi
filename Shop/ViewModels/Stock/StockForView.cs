﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.Stock
{
    public class StockForView
    {
        public int StockId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Poster { get; set; }
        public CategoryStock Category { get; set; }
        public BrandStock Brand { get; set; }
        public long Quantity { get; set; }
    }
    public class CategoryStock
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class BrandStock
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
    }
}
