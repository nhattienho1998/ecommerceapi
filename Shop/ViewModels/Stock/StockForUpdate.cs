﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.Stock
{
    public class StockForUpdate
    {
        public long Quantity { get; set; }
    }
}
