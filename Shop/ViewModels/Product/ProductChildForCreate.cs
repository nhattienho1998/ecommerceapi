﻿using Microsoft.AspNetCore.Http;
using Shop.ViewModels.AttributeValue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.Product
{
    public class ProductChildForCreate
    {
        public int ParentId { get; set; }
        public decimal Price { get; set; }
        public string Poster { get; set; }
        public string SKU { get; set; }
        public ICollection<AttributeValueForCreate> ListAtrribute { get; set; }
    }
}
