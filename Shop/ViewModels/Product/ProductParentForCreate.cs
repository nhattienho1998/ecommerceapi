﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.Product
{
    public class ProductParentForCreate
    {
        [Required]
        public decimal Price { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string SKU { get; set; }
        [Required]
        public int BrandId { get; set; }
        [Required]
        public string Unit { get; set; }
        [Required]
        public string Origin { get; set; }
        public string ShortDescription { get; set; }
        public string Link { get; set; }
        public string Poster { get; set; }
        public bool IsActive { get; set; }
        public bool IsTrending { get; set; }
    }
}
