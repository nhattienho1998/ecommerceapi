﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.ImageProduct
{
    public class ImageProductForCreate 
    {
        public string File { get; set; }
        public int ProductId { get; set; }
    }
}
