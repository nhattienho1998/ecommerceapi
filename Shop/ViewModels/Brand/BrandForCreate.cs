﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.Brand
{
    public class BrandForCreate
    {
        public string Name { get; set; }
        public string Link { get; set; }
    }
}
