﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.AttributeProduct
{
    public class AttributeForList : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
