﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels.AttributeProduct
{
    public class AttributeProductForCreate
    {
        public string Name { get; set; }
    }
}
