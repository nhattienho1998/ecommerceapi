﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Authentication.FaceBook
{
    public class FacebookLoginResource
    {
        [Required]
        public string FacebookToken { get; set; }
    }
}
