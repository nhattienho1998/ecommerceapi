﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Authentication.Facebook
{
    public interface IFacebookService
    {
        Task<FacebookUserResource> GetUserFromFacebookAsync(string facebookToken);
    }
}
