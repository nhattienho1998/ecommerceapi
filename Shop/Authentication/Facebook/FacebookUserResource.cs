﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Authentication.Facebook
{
    public class FacebookUserResource
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string FacebookId { get; set; }
    }
}
