﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shop.Data;
using Shop.Models;
using Shop.ViewModels;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Authentication
{
    public static class TokenProvider
    {
       
        public static async Task<string> GenerateToken(User user)
        {
            string secretKey = "my_name_is_ho_nguyen_nhat_tien";//line 1

            SymmetricSecurityKey SigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            var identity = await Task.Run(()=>GetIdentity(user));
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                claims: identity.Claims,
                notBefore: now,
                signingCredentials: new SigningCredentials(SigningKey, SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
        private async static Task<ClaimsIdentity> GetIdentity(User user)
        {
           

            if (user == null) return null;


            IList<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString(), null, ClaimsIdentity.DefaultIssuer, "Provider"));

            claims.Add(new Claim(ClaimTypes.Role, user.Role, null, ClaimsIdentity.DefaultIssuer, "Provider"));

            return await Task.FromResult(new ClaimsIdentity(claims, "Bearer"));
        }
    }
}
