﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.Extensions.Options;
using Shop.Data.GoogleCloudStorage;
using Shop.Helpers;
using Shop.Models;
using Shop.ViewModels.ImageProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Image
{
    public class ServiceImage : IServiceImage
    {
        private readonly ICloudStorage _cloudStorage;
        private readonly DataContext _context;

        public ServiceImage(DataContext context, ICloudStorage cloudStorage)
        {
            _context = context;
            _cloudStorage = cloudStorage;
        }
        public async Task<ImageProduct> AddImage(ImageProductForCreate image)
        {
            var product = _context.Products.FirstOrDefault(x => x.ProductId == image.ProductId);
            var strName = "";
            if(product.ParentId == 0 || product.ParentId == null)
            {
                strName = product.Link;
            }
            else
            {
                strName = _context.Products.FirstOrDefault(x => x.ProductId == product.ParentId).Link;
            }
            var newImage = new ImageProduct
            {
                ProductId = image.ProductId,
                DateAdded = DateTime.Now,
                Url = await _cloudStorage.UploadFileAsync(image.File, strName)
            };
            await _context.AddAsync(newImage);
            await _context.SaveChangesAsync();
            return newImage;
        }

        public async Task<object> AddMultipleImage(ICollection<ImageProductForCreate> listImages)
        {
            var listProducts = _context.Products.ToList();
            var result = new List<object>();
            var transaction = _context.Database.BeginTransaction();
            try
            {
                foreach (var image in listImages)
                {
                    var product = listProducts.FirstOrDefault(x => x.ProductId == image.ProductId);
                    var strName = "";
                    if (product.ParentId == 0 || product.ParentId == null)
                    {
                        strName = product.Link;
                    }
                    else
                    {
                        strName = _context.Products.FirstOrDefault(x => x.ProductId == product.ParentId).Link;
                    }
                    var newImage = new ImageProduct
                    {
                        ProductId = image.ProductId,
                        DateAdded = DateTime.Now,
                        Url = await _cloudStorage.UploadFileAsync(image.File, strName)
                    };
                    await _context.AddAsync(newImage);
                    await _context.SaveChangesAsync();
                    var imageResult = new
                    {
                        Id = newImage.ImageId,
                        ProductId = newImage.ProductId,
                        Url = newImage.Url
                    };
                    result.Add(imageResult);
                }
                transaction.Commit();
                return result;
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                Console.WriteLine("Error occurred.");
                return null;
            }
        }
    }
}
