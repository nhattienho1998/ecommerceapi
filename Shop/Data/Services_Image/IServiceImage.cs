﻿using Shop.Models;
using Shop.ViewModels.ImageProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Image
{
    public interface IServiceImage
    {
        Task<ImageProduct> AddImage(ImageProductForCreate image);
        Task<object> AddMultipleImage(ICollection<ImageProductForCreate> listImages);
    }
}
