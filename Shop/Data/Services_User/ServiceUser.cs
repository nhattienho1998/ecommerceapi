﻿using DocumentFormat.OpenXml.Office.CustomXsn;
using Microsoft.EntityFrameworkCore;
using Shop.Authentication.Facebook;
using Shop.Authentication.FaceBook;
using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Data.Services_User
{
    public class ServiceUser : IServiceUser
    {
        private readonly DataContext _context;
        public ServiceUser(DataContext context)
        {
            _context = context;
        }
        public async Task<User> SignUp(UserForCreate user)
        {
            var newUser = new User
            {
                Name = user.Name,
                Password = EncodePasswordToBase64(user.Password),
                Email = user.Email,
                Role = "Customer",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Status = "Actived"
            };
            await _context.Users.AddAsync(newUser);
            await _context.SaveChangesAsync();
            return newUser;
        }

        public async Task<User> Login(UserForLogin user)
        {
            var password = EncodePasswordToBase64(user.Password);
            var result = await _context.Users.FirstOrDefaultAsync(x => x.Email == user.Email && x.Status == "Actived");
            if (result == null)
            {
                return null;
            }
            else
            {
                if (password == result.Password)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }


        public async Task<User> LoginWithFacebook(FacebookLoginResource facebookLoginResource)
        {
            var serviceFacebook = new FacebookService();
            var facebookUser = await serviceFacebook.GetUserFromFacebookAsync(facebookLoginResource.FacebookToken);
            var result = _context.Users.FirstOrDefault(x => x.Email == facebookUser.Email);
            if (result == null)
            {
                var newUser = new User
                {
                    Name = facebookUser.Name,
                    Email = facebookUser.Email,
                    Role = "Customer",
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    Status = "Actived"
                };

                await _context.Users.AddAsync(newUser);
                await _context.SaveChangesAsync();
                return newUser;
            }
            else
            {
                return result;
            }
        }

        public string EncodePasswordToBase64(string password)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);
            byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
            return Convert.ToBase64String(inArray);
        }

        public ValidationResultViewModel ValidationBeforeCreate(UserForCreate user)
        {
            var totalEmail = _context.Users.Count(x => x.Email == user.Email);

            IDictionary<string, string> Errors = new Dictionary<string, string>();
            if (totalEmail >= 1)
            {
                Errors.Add("UserName", "User name is duplicated!");
                return new ValidationResultViewModel
                {
                    IsValid = false,
                    Errors = Errors
                };
            }
            else
            {
                return new ValidationResultViewModel
                {
                    IsValid = true
                };
            }
        }


        public async Task<User> GetUserInfo(int id)
        {
            var result = await _context.Users.FirstOrDefaultAsync(x => x.UserId == id);
            return result;
        }

        public async Task<User> ChangePassword(string oldpassword, UserForChangePassword user, int id)
        {
            var userForChangePassword = await _context.Users.AsNoTracking().FirstOrDefaultAsync(x => x.UserId == id);
            if (EncodePasswordToBase64(oldpassword) == userForChangePassword.Password)
            {
                userForChangePassword.Password = EncodePasswordToBase64(user.Password);
                return userForChangePassword;
            }
            else
            {
                return null;
            }
        }


        public async Task<User> CreateStaff(UserForCreateStaff staff)
        {
            var newStaff = new User
            {
                Name = staff.Name,
                Password = EncodePasswordToBase64(staff.Password),
                Email = staff.Email,
                Role = "Staff",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Status = "Actived"
            };
            await _context.AddAsync(newStaff);
            await _context.SaveChangesAsync();
            return newStaff;
        }

        public ValidationResultViewModel ValidationBeforeCreateStaff(UserForCreateStaff staff)
        {
            var totalEmail = _context.Users.Count(x => x.Email == staff.Email);

            IDictionary<string, string> Errors = new Dictionary<string, string>();


            if (totalEmail >= 1)
            {
                Errors.Add("Email", "Email is duplicated!");
                return new ValidationResultViewModel
                {
                    IsValid = false,
                    Errors = Errors
                };
            }
            else
            {
                return new ValidationResultViewModel
                {
                    IsValid = true
                };
            }
        }
    }
}
