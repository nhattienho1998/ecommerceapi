﻿using Shop.Authentication.FaceBook;
using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_User
{
    public interface IServiceUser
    {
        Task<User> SignUp(UserForCreate user);
        Task<User> Login(UserForLogin user);
        Task<User> GetUserInfo(int id);
        ValidationResultViewModel ValidationBeforeCreate(UserForCreate user);
        Task<User> ChangePassword(string oldpassword, UserForChangePassword user, int id);
        Task<User> CreateStaff(UserForCreateStaff staff);
        Task<User> LoginWithFacebook(FacebookLoginResource facebookLoginResource);
        ValidationResultViewModel ValidationBeforeCreateStaff(UserForCreateStaff staff);
    }
}
