﻿using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.AttributeProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Attribute
{
    public interface IServiceAttributeProduct
    {
        Task<AttributeProduct> Create(AttributeProductForCreate attribute);
        Task<AttributeProduct> Update(AttributeProductForUpdate attribute);
        Task<List<object>> GetAll();
        ValidationResultViewModel ValidationBeforeCreate(AttributeProductForCreate attribute);
        Task<object> AddValue(int id, ValueForCreate value);
        Task<object> GetValues(int id);
    }
}
