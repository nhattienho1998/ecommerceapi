﻿using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.AttributeProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Attribute
{
    public class ServiceAttributeProduct : IServiceAttributeProduct
    {
        private readonly DataContext _context;
        public ServiceAttributeProduct(DataContext context)
        {
            _context = context;
        }

        public async Task<object> AddValue(int id, ValueForCreate value)
        {
            var attribute = _context.AttributeProduct.FirstOrDefault(x => x.AttributeId == id);
            var arrayValue = value.Value.Split(',');
            var listValue = new List<object>();
            var transaction = _context.Database.BeginTransaction();
            try
            {
                foreach (var item in arrayValue)
                {
                    var newValue = new AttributeValue
                    {
                        AttributeId = id,
                        Value = item.Trim(),
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        Status = "Actived"
                    };
                    await _context.AttributeValues.AddAsync(newValue);
                    await _context.SaveChangesAsync();
                    var temp = new
                    {
                        ValueId = newValue.Id,
                        Value = newValue.Value,
                        CreateDate = newValue.CreateDate,
                        UpdateDate = newValue.UpdateDate,
                        Status = newValue.Status
                    };
                    listValue.Add(temp);
                }
                transaction.Commit();
                return listValue;
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                return null;
            }
            
        }


        public async Task<AttributeProduct> Create(AttributeProductForCreate attribute)
        {
            var newAttribute = new AttributeProduct
            {
                Name = attribute.Name,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Status = "Actived"
            };
            await _context.AttributeProduct.AddAsync(newAttribute);
            await _context.SaveChangesAsync();
            return newAttribute;
        }

        public async Task<List<object>> GetAll()
        {
            var result = _context.AttributeProduct.OrderByDescending(x => x.CreateDate);
            var listItemTemp = _context.AttributeValues.ToList();
            var listAttribute = new List<object>();
            foreach (var item in result)
            {
                var listTemp = listItemTemp.Where(x => x.AttributeId == item.AttributeId);
                var listItem = new List<string>();
                foreach(var value in listTemp)
                {
                    listItem.Add(value.Value);
                }
                
                var newItem = new 
                {
                    Id = item.AttributeId,
                    Name = item.Name,
                    CreateDate = item.CreateDate,
                    UpdateDate = item.UpdateDate,
                    Status = item.Status,
                    ListItem = listItem
                };
                listAttribute.Add(newItem);
            }

            return listAttribute;
        }

        public async Task<object> GetValues(int id)
        {
            var listValues = _context.AttributeValues.Where(x=>x.AttributeId == id);
            var result = new List<string>();
            foreach(var item in listValues)
            {
                result.Add(item.Value);
            }
            return result;
        }

        public Task<AttributeProduct> Update(AttributeProductForUpdate attribute)
        {
            throw new NotImplementedException();
        }

        public ValidationResultViewModel ValidationBeforeCreate(AttributeProductForCreate attribute)
        {
            var totalName = _context.AttributeProduct.Count(x=>x.Name == attribute.Name);
            IDictionary<string, string> Errors = new Dictionary<string, string>();
            if (totalName >= 1)
            {
                Errors.Add("Name", "Name is duplicated!");
                return new ValidationResultViewModel
                {
                    IsValid = false,
                    Errors = Errors
                };
            }
            else
            {
                return new ValidationResultViewModel
                {
                    IsValid = true
                };
            }
        }

    }
}
