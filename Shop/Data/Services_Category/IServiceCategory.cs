﻿using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Category
{
    public interface IServiceCategory
    {
        Task<Object> Create(CategoryForCreate category);
        ValidationResultViewModel ValidationBeforeCreate(CategoryForCreate category);
        Task<object> GetParentCategory();
        Task<object> GetTreeCategory();
        Task<object> GetChildCategory(int id);
        Task<object> GetAll();
        Task<object> GetAllChild();
        Task<object> Update(int id, CategoryForUpdate category);
        ValidationResultViewModel ValidationBeforeUpdate(int id,CategoryForUpdate category);
    }
}
