﻿using Microsoft.EntityFrameworkCore;
using Shop.Data.GoogleCloudStorage;
using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Category
{
    public class ServiceCategory : IServiceCategory
    {
        private readonly DataContext _context;
        private readonly ICloudStorage _cloudStorage;
        public ServiceCategory(DataContext context, ICloudStorage cloudStorage)
        {
            _context = context;
            _cloudStorage = cloudStorage;
        }
        public async Task<Object> Create(CategoryForCreate category)
        {
            var parentCategory = await _context.Categories.FirstOrDefaultAsync(x => x.CategoryId == category.ParentId);
            var CategoryParent = new Object();
            if (parentCategory == null)
            {
                CategoryParent = null;
            }
            else
            {
                CategoryParent = new
                {
                    CategoryId = parentCategory.CategoryId,
                    CategoryName = parentCategory.Name,
                    Poster = parentCategory.Poster,
                    Description = parentCategory.Description,
                    Link = parentCategory.Link
                };
            }
            var newCategory = new Category
            {
                Name = category.Name,
                ParentId = category.ParentId,
                Poster = await _cloudStorage.UploadFileAsync(category.Poster, category.Link),
                Description = category.Description,
                Link = category.Link,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Status = "Actived"
            };
            await _context.Categories.AddAsync(newCategory);
            await _context.SaveChangesAsync();
            return new
            {
                CategoryId = newCategory.CategoryId,
                CategoryParent = CategoryParent,
                Poster = newCategory.Poster,
                Name = newCategory.Name,
                Description = newCategory.Description,
                Link = newCategory.Link,
                CreateDate = newCategory.CreateDate,
                UpdateDate = newCategory.UpdateDate,
                Status = newCategory.Status
            };
        }

        public async Task<object> GetChildCategory(int id)
        {
            var listChildCategory = _context.Categories.Where(x => x.ParentId == id);
            var result = new List<object>();
            if (listChildCategory == null)
            {
                return null;
            }
            foreach (var item in listChildCategory)
            {
                var category = new
                {
                    CategoryID = item.CategoryId,
                    Name = item.Name,
                    Poster = item.Poster,
                    ParentId = item.ParentId,
                    Description = item.Description,
                    Link = item.Link,
                    Status = item.Status,
                    CreateDate = item.CreateDate,
                    UpdateDate = item.UpdateDate
                };
                result.Add(category);
            }
            return result;
        }

        public async Task<object> GetParentCategory()
        {
            var listParent = _context.Categories.Where(x => x.ParentId == 0 || x.ParentId == null);
            var result = new List<object>();
            foreach (var item in listParent)
            {
                var category = new
                {
                    CategoryID = item.CategoryId,
                    Name = item.Name,
                    Poster = item.Poster,
                    ParentId = item.ParentId,
                    Description = item.Description,
                    Link = item.Link,
                    Status = item.Status,
                    CreateDate = item.CreateDate,
                    UpdateDate = item.UpdateDate
                };
                result.Add(category);
            }
            return result;
        }

        public async Task<object> GetTreeCategory()
        {
            var listChildren = _context.Categories.Where(x => x.ParentId != null && x.ParentId != 0).ToList();
            var listParent = _context.Categories.Where(x => x.ParentId == 0 || x.ParentId == null).ToList();
            var result = new List<object>();
            foreach (var item in listParent)
            {
                var listChild = listChildren.Where(x => x.ParentId == item.CategoryId);
                var Children = new List<object>();
                foreach (var children in listChild)
                {
                    var childrenItem = new
                    {
                        CategoryId = children.CategoryId,
                        Name = children.Name,
                        Poster = children.Poster,
                        ParentId = children.ParentId,
                        Description = children.Description,
                        Link = children.Link,
                        Status = children.Status
                    };
                    Children.Add(childrenItem);
                }
                var parentItem = new
                {
                    CategoryId = item.CategoryId,
                    Name = item.Name,
                    Poster = item.Poster,
                    ParentId = item.ParentId,
                    Description = item.Description,
                    Link = item.Link,
                    Status = item.Status,
                    Children = Children
                };
                result.Add(parentItem);
            }
            return result;
        }

        public ValidationResultViewModel ValidationBeforeCreate(CategoryForCreate category)
        {
            var totalName = _context.Categories.Count(x => x.Name == category.Name);
            var totalLink = _context.Categories.Count(x => x.Link == category.Link);
            IDictionary<string, string> Errors = new Dictionary<string, string>();
            if (totalName > 0 || totalLink > 0)
            {
                if (totalName > 0)
                {
                    Errors.Add("Name", "Name is duplicated");
                }
                if (totalLink > 0)
                {
                    Errors.Add("Link", "Link is duplicated");
                }
                return new ValidationResultViewModel
                {
                    IsValid = false,
                    Errors = Errors
                };
            }
            else
            {
                return new ValidationResultViewModel
                {
                    IsValid = true
                };
            }
        }
        public async Task<object> GetAll()
        {
            var listTemp = _context.Categories.OrderBy(x => x.CategoryId);
            var result = new List<object>();
            foreach (var item in listTemp)
            {
                var category = new
                {
                    CategoryID = item.CategoryId,
                    Name = item.Name,
                    Poster = item.Poster,
                    ParentId = item.ParentId,
                    Description = item.Description,
                    Link = item.Link,
                    Status = item.Status,
                    CreateDate = item.CreateDate,
                    UpdateDate = item.UpdateDate
                };
                result.Add(category);
            }
            return result;
        }

        public async Task<object> GetAllChild()
        {
            var listChild = _context.Categories.Where(x => x.ParentId != 0 && x.ParentId != null);
            var result = new List<object>();
            foreach (var item in listChild)
            {
                var category = new
                {
                    CategoryID = item.CategoryId,
                    Name = item.Name,
                    Poster = item.Poster,
                    ParentId = item.ParentId,
                    Description = item.Description,
                    Link = item.Link,
                    Status = item.Status,
                    CreateDate = item.CreateDate,
                    UpdateDate = item.UpdateDate
                };
                result.Add(category);
            }
            return result;
        }

        public async Task<object> Update(int id, CategoryForUpdate category)
        {
            var oldRecord = _context.Categories.AsNoTracking().FirstOrDefault(x => x.CategoryId == id);
            var nameFile = oldRecord.Poster.Substring(55);
            var categoryToUpdate = new Category
            {
                CategoryId = id,
                ParentId = category.ParentId,
                Description = category.Description,
                Name = category.Name,
                Link = category.Link,
                UpdateDate = DateTime.Now,
                CreateDate = oldRecord.CreateDate,
                Status = oldRecord.Status
            };
            if (string.IsNullOrEmpty(category.Poster))
            {
                categoryToUpdate.Poster = oldRecord.Poster;
            }
            else
            {
                categoryToUpdate.Poster = await _cloudStorage.UploadFileAsync(category.Poster, category.Link);
                await _cloudStorage.DeleteFileAsync(nameFile);
            }

            _context.Categories.Update(categoryToUpdate);
            await _context.SaveChangesAsync();
            return new
            {
                CategoryId = categoryToUpdate.CategoryId,
                ParentId = categoryToUpdate.ParentId,
                Name = categoryToUpdate.Name,
                Poster = categoryToUpdate.Poster,
                Description = categoryToUpdate.Description,
                Link = categoryToUpdate.Link,
                CreateDate = categoryToUpdate.CreateDate,
                UpdateDate = categoryToUpdate.UpdateDate,
                Status = categoryToUpdate.Status
            };
        }

        public ValidationResultViewModel ValidationBeforeUpdate(int id, CategoryForUpdate category)
        {
            var totalName = _context.Categories.Count(x => x.CategoryId != id && x.Name == category.Name);
            var totalLink = _context.Categories.Count(x => x.CategoryId != id && x.Link == category.Link);
            IDictionary<string, string> Errors = new Dictionary<string, string>();
            if (totalName > 0 || totalLink > 0)
            {
                if (totalName > 0)
                {
                    Errors.Add("Name", "Name is duplicated");
                }
                if (totalLink > 0)
                {
                    Errors.Add("Link", "Link is duplicated");
                }
                return new ValidationResultViewModel
                {
                    IsValid = false,
                    Errors = Errors
                };
            }
            else
            {
                return new ValidationResultViewModel
                {
                    IsValid = true
                };
            }
        }
    }
}
