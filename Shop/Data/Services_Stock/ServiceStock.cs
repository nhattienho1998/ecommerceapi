﻿using Microsoft.EntityFrameworkCore;
using Shop.Helpers;
using Shop.Helpers.Params;
using Shop.ViewModels.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Stock
{
    public class ServiceStock : IServiceStock
    {
        private readonly DataContext _context;
        public ServiceStock(DataContext context)
        {
            _context = context;
        }
        public async Task<object> GetAll(StockParams stockParams)
        {
            var keyword = stockParams.Keyword;
            var categoryId = stockParams.Category;
            var brandId = stockParams.Brand;
            var listProducts = _context.Products.ToList();
            var listStocks = _context.Stocks.ToList();
            var listCategories = _context.Categories.ToList();
            var listBrands = _context.Brands.ToList();
            var temp = new List<StockForView>();
            foreach (var item in listStocks)
            {
                var product = listProducts.FirstOrDefault(x => x.ProductId == item.ProductId);
                var brand = listBrands.FirstOrDefault(x => x.BrandId == product.BrandId);
                var category = listCategories.FirstOrDefault(x => x.CategoryId == product.CategoryId);
                var Brand = new BrandStock();
                var Category = new CategoryStock();
                if (brand == null)
                {
                    Brand = null;
                }
                else
                {
                    Brand = new BrandStock
                    {
                        BrandId = brand.BrandId,
                        BrandName = brand.Name
                    };
                }

                if (category == null)
                {
                    Category = null;
                }
                else
                {
                    Category = new CategoryStock
                    {
                        CategoryId = category.CategoryId,
                        CategoryName = category.Name
                    };
                }
                var stock = new StockForView
                {
                    StockId = item.StockId,
                    Brand = Brand,
                    Category = Category,
                    ProductId = product.ProductId,
                    ProductName = product.Name,
                    Quantity = item.Quantity,
                    Poster = product.Poster
                };
                temp.Add(stock);
            }

            if (categoryId != null && categoryId > 0)
            {
                temp = temp.Where(x => x.Category.CategoryId == categoryId).ToList();
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                temp = temp.Where(x => x.ProductName.ToLower().Contains(keyword.ToLower())).ToList();
            }
            if (brandId != null && brandId > 0)
            {
                temp = temp.Where(x => x.Brand.BrandId == brandId).ToList();
            }
            var tempResult = temp.AsQueryable();
            var totalItems = tempResult.Count();
            var totalPages = (int)Math.Ceiling((double)totalItems / (double)stockParams.PageSize);
            var result = PagedList<StockForView>.Create(tempResult, stockParams.PageNumber, stockParams.PageSize);
            return new
            {
                listStocks = result,
                totalPages = totalPages,
                totalItems = totalItems,
                pageNumber = stockParams.PageNumber
            };
        }

        public async Task<object> UpdateStock(int id, StockForUpdate stock)
        {
            var oldRecord = _context.Stocks.AsNoTracking().FirstOrDefault(x => x.StockId == id);
            var product = _context.Products.FirstOrDefault(x => x.ProductId == oldRecord.ProductId);
            var brand = _context.Brands.FirstOrDefault(x => x.BrandId == product.BrandId);
            var category = _context.Categories.FirstOrDefault(x => x.CategoryId == product.CategoryId);
            var Brand = new BrandStock();
            var Category = new CategoryStock();
            if (brand == null)
            {
                Brand = null;
            }
            else
            {
                Brand = new BrandStock
                {
                    BrandId = brand.BrandId,
                    BrandName = brand.Name
                };
            }

            if (category == null)
            {
                Category = null;
            }
            else
            {
                Category = new CategoryStock
                {
                    CategoryId = category.CategoryId,
                    CategoryName = category.Name
                };
            }
            oldRecord.Quantity = stock.Quantity;
            if (stock.Quantity > 0)
            {
                oldRecord.Status = "Còn hàng";
            }
            else
            {
                oldRecord.Status = "Hết hàng";
            }
            oldRecord.UpdateDate = DateTime.Now;
            _context.Stocks.Update(oldRecord);
            await _context.SaveChangesAsync();
            var result = new StockForView
            {
                StockId = oldRecord.StockId,
                Brand = Brand,
                Category = Category,
                ProductId = product.ProductId,
                ProductName = product.Name,
                Poster = product.Poster,
                Quantity = oldRecord.Quantity
            };
            return result;
        }
    }
}
