﻿using Shop.Helpers.Params;
using Shop.ViewModels.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Stock
{
    public interface IServiceStock
    {
        Task<object> GetAll(StockParams stockParams);
        Task<object> UpdateStock(int id, StockForUpdate stock);
    }
}
