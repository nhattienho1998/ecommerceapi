﻿using Shop.ViewModels;
using Shop.ViewModels.Brand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Brand
{
    public interface IServiceBrand
    {
        Task<object> GetAll();
        Task<object> Create(BrandForCreate brand);
        Task<object> Update();
        ValidationResultViewModel ValidationBeforeCreate(BrandForCreate brand);
    }
}
