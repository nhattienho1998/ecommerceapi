﻿using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.Brand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Brand
{
    public class ServiceBrand : IServiceBrand
    {
        private readonly DataContext _context;
        public ServiceBrand(DataContext context)
        {
            _context = context;
        }
        public async Task<object> Create(BrandForCreate brand)
        {
            var newBrand = new Brand
            {
                Name = brand.Name,
                Link = brand.Link,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Status = "Actived"
            };
            await _context.Brands.AddAsync(newBrand);
            await _context.SaveChangesAsync();
            return new
            {
                BrandId = newBrand.BrandId,
                Name = newBrand.Name,
                Link = newBrand.Link,
                CreateDate = newBrand.CreateDate,
                UpdateDate = newBrand.UpdateDate,
                Status = newBrand.Status
            };
        }

        public async Task<object> GetAll()
        {
            var listBrand = _context.Brands.ToList();
            var result = new List<object>();
            foreach (var item in listBrand)
            {
                var brand = new
                {
                    BrandId = item.BrandId,
                    Name = item.Name,
                    Link = item.Link,
                    CreateDate = item.CreateDate,
                    UpdateDate = item.UpdateDate,
                    Status = item.Status
                };
                result.Add(brand);
            }
            return result;
        }

        public Task<object> Update()
        {
            throw new NotImplementedException();
        }

        public ValidationResultViewModel ValidationBeforeCreate(BrandForCreate brand)
        {
            var totalName = _context.Brands.Count(x => x.Name == brand.Name);
            var totalLink = _context.Brands.Count(x => x.Link == brand.Link);
            IDictionary<string, string> Errors = new Dictionary<string, string>();
            if (totalName > 0 || totalLink > 0)
            {
                if (totalName > 0)
                {
                    Errors.Add("Name", "Name is duplicated");
                }
                if (totalLink > 0)
                {
                    Errors.Add("Link", "Link is duplicated");
                }
                return new ValidationResultViewModel
                {
                    IsValid = false,
                    Errors = Errors
                };
            }
            else
            {
                return new ValidationResultViewModel
                {
                    IsValid = true
                };
            }
        }
    }
}
