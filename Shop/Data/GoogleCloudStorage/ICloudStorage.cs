﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.GoogleCloudStorage
{
    public interface ICloudStorage
    {
        Task<string> UploadFileAsync(string imageFile, string imageName);
        Task DeleteFileAsync(string fileNameForStorage);
    }
}
