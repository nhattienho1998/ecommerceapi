﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.GoogleCloudStorage
{
    public class GoogleCloudStorage : ICloudStorage
    {
        private readonly GoogleCredential googleCredential;
        private readonly StorageClient storageClient;
        private readonly string bucketName;
        public GoogleCloudStorage(IConfiguration configuration)
        {
            googleCredential = GoogleCredential.FromFile(configuration.GetValue<string>("GoogleCredentialFile"));
            storageClient = StorageClient.Create(googleCredential);
            bucketName = configuration.GetValue<string>("GoogleCloudStorageBucket");
        }
        public async Task DeleteFileAsync(string fileNameForStorage)
        {
            await storageClient.DeleteObjectAsync(bucketName, fileNameForStorage);
        }

        public async Task<string> UploadFileAsync(string imageFile, string imageName)
        {
            var bytes = Convert.FromBase64String(imageFile);
            var imageData = new MemoryStream(bytes);
            imageData.Position = 0;
            var uuid = Guid.NewGuid().ToString();
            var fileName = uuid + imageName+".png";
            var dataObject = await storageClient.UploadObjectAsync(bucketName, fileName, null, imageData);
            return "https://storage.googleapis.com/bucket_ecommerce_images/" + fileName;
            //using (var memoryStream = new System.IO.MemoryStream())
            //{
            //    var uuid = Guid.NewGuid().ToString();
            //    var fileName = uuid + imageName;
            //    Image
            //    var dataObject = await storageClient.UploadObjectAsync(bucketName, fileName, null, memoryStream);
            //    return "https://storage.googleapis.com/bucket_ecommerce_images/"+fileName;
            //}
        }
    }
}
