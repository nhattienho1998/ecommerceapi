﻿using Microsoft.EntityFrameworkCore;
using Shop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data
{
    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            :base(options)
        { 
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Ward> Wards { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ImageProduct> ImageProducts { get; set; }
        public DbSet<AttributeProduct> AttributeProduct { get; set; }
        public DbSet<AttributeProductValue> AttributeProductValues { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<DiscountCode> DiscountCodes { get; set; }
        public DbSet<DiscountProduct> DiscountProducts { get; set; }
        public DbSet<AttributeValue> AttributeValues { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartDetail> CartDetails { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //keys
            modelBuilder.Entity<User>().HasKey(x => x.UserId);
            modelBuilder.Entity<User>().Property(x => x.UserId).ValueGeneratedOnAdd();
            modelBuilder.Entity<Province>().HasKey(x => x.ProvinceId);
            modelBuilder.Entity<District>().HasKey(x => x.DistrictId);
            modelBuilder.Entity<Ward>().HasKey(x => x.Id);
            modelBuilder.Entity<Product>().HasKey(x => x.ProductId);
            modelBuilder.Entity<Product>().Property(x => x.ProductId).ValueGeneratedOnAdd();
            modelBuilder.Entity<Category>().HasKey(x => x.CategoryId);
            modelBuilder.Entity<Category>().Property(x => x.CategoryId).ValueGeneratedOnAdd();
            modelBuilder.Entity<ImageProduct>().HasKey(x => x.ImageId);
            modelBuilder.Entity<ImageProduct>().Property(x => x.ImageId).ValueGeneratedOnAdd();
            modelBuilder.Entity<AttributeProduct>().HasKey(x => x.AttributeId);
            modelBuilder.Entity<AttributeProduct>().Property(x => x.AttributeId).ValueGeneratedOnAdd();
            modelBuilder.Entity<AttributeProductValue>().HasKey(x => x.AttibuteValueId);
            modelBuilder.Entity<AttributeProductValue>().Property(x => x.AttibuteValueId).ValueGeneratedOnAdd();
            modelBuilder.Entity<Stock>().HasKey(x => x.StockId);
            modelBuilder.Entity<Stock>().Property(x => x.StockId).ValueGeneratedOnAdd();
            modelBuilder.Entity<Order>().HasKey(x => x.OrderId);
            modelBuilder.Entity<Order>().Property(x => x.OrderId).ValueGeneratedOnAdd();
            modelBuilder.Entity<OrderDetail>().HasKey(x => x.OrderDetailId);
            modelBuilder.Entity<OrderDetail>().Property(x => x.OrderDetailId).ValueGeneratedOnAdd();
            modelBuilder.Entity<DiscountProduct>().HasKey(x => x.DiscountProductId);
            modelBuilder.Entity<DiscountProduct>().Property(x => x.DiscountProductId).ValueGeneratedOnAdd();
            modelBuilder.Entity<DiscountCode>().HasKey(x => x.DiscountCodeId);
            modelBuilder.Entity<DiscountCode>().Property(x => x.DiscountCodeId).ValueGeneratedOnAdd();
            modelBuilder.Entity<Brand>().HasKey(x => x.BrandId);
            modelBuilder.Entity<Brand>().Property(x => x.BrandId).ValueGeneratedOnAdd();
            modelBuilder.Entity<AttributeValue>().HasKey(x => x.Id);
            modelBuilder.Entity<AttributeValue>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Cart>().HasKey(x => x.CartId);
            modelBuilder.Entity<Cart>().Property(x => x.CartId).ValueGeneratedOnAdd();
            modelBuilder.Entity<CartDetail>().HasKey(x => x.Id);
            modelBuilder.Entity<CartDetail>().Property(x => x.Id).ValueGeneratedOnAdd();
            //Requirements
            modelBuilder.Entity<User>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.Status).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.Email).IsRequired();
            modelBuilder.Entity<User>().HasIndex(x => x.Email).IsUnique();
            modelBuilder.Entity<Province>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Province>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<District>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Category>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Category>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<Category>().HasIndex(x => x.Link).IsUnique();
            modelBuilder.Entity<Product>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Product>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<Product>().Property(x => x.Price).IsRequired();
            modelBuilder.Entity<Product>().Property(x => x.SKU).IsRequired();
            modelBuilder.Entity<Product>().HasIndex(x => x.SKU).IsUnique();
            modelBuilder.Entity<Product>().HasIndex(x => x.Link).IsUnique();
            modelBuilder.Entity<AttributeProduct>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<AttributeProduct>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<AttributeProductValue>().Property(x => x.Value).IsRequired();
            modelBuilder.Entity<AttributeProductValue>().Property(x => x.ProductId).IsRequired();
            modelBuilder.Entity<AttributeProductValue>().HasIndex(x => new {x.Value,x.ProductId,x.AttributeId });
            modelBuilder.Entity<AttributeProductValue>().Property(x => x.AttributeId).IsRequired();
            modelBuilder.Entity<Stock>().HasIndex(x => x.ProductId).IsUnique();
            modelBuilder.Entity<Stock>().Property(x => x.ProductId).IsRequired();
            modelBuilder.Entity<Stock>().Property(x => x.Quantity).IsRequired();
            modelBuilder.Entity<DiscountCode>().HasIndex(x => x.Code).IsUnique();
            modelBuilder.Entity<DiscountCode>().Property(x => x.Code).IsRequired();
            modelBuilder.Entity<DiscountCode>().Property(x => x.Value).IsRequired();
            modelBuilder.Entity<DiscountCode>().Property(x => x.StartDate).IsRequired();
            modelBuilder.Entity<DiscountCode>().Property(x => x.EndDate).IsRequired();
            modelBuilder.Entity<DiscountCode>().Property(x => x.QuantityOrder).IsRequired();
            modelBuilder.Entity<DiscountProduct>().Property(x => x.Value).IsRequired();
            modelBuilder.Entity<DiscountProduct>().Property(x => x.ProductId).IsRequired();
            modelBuilder.Entity<DiscountProduct>().Property(x => x.StartDate).IsRequired();
            modelBuilder.Entity<DiscountProduct>().Property(x => x.EndDate).IsRequired();
            modelBuilder.Entity<Brand>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Brand>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<Brand>().Property(x => x.Link).IsRequired();
            modelBuilder.Entity<Brand>().HasIndex(x => x.Link).IsUnique();
            modelBuilder.Entity<AttributeValue>().Property(x => x.AttributeId).IsRequired();
            modelBuilder.Entity<AttributeValue>().Property(x => x.Value).IsRequired();
            modelBuilder.Entity<AttributeValue>().HasIndex(x => new { x.AttributeId, x.Value });
            modelBuilder.Entity<Cart>().Property(x => x.TokenCart).IsRequired();
            modelBuilder.Entity<Cart>().HasIndex(x => x.TokenCart).IsUnique();


            //Relationships
            modelBuilder.Entity<Ward>().HasOne(x => x.District)
                                       .WithMany(r => r.Wards)
                                       .HasForeignKey(x => x.DistrictId)
                                       .IsRequired();

            modelBuilder.Entity<District>().HasOne(x => x.Province)
                                           .WithMany(r => r.Districts)
                                           .HasForeignKey(x => x.ProvinceId)
                                           .IsRequired();
            modelBuilder.Entity<Product>().HasOne(x => x.Category)
                                          .WithMany(r => r.Products)
                                          .HasForeignKey(x => x.CategoryId);
            modelBuilder.Entity<Product>().HasOne(x => x.Brand)
                                          .WithMany(r => r.Products)
                                          .HasForeignKey(x => x.BrandId);

            modelBuilder.Entity<ImageProduct>().HasOne(x => x.Product)
                                               .WithMany(r => r.ImageProducts)
                                               .HasForeignKey(x => x.ProductId)
                                               .IsRequired();

            modelBuilder.Entity<AttributeProductValue>().HasOne(x => x.AttributeProduct)
                                                        .WithMany(r => r.AttributeProductValues)
                                                        .HasForeignKey(x => x.AttributeId)
                                                        .IsRequired();
            modelBuilder.Entity<AttributeProductValue>().HasOne(x => x.Product)
                                                        .WithMany(r => r.AttributeProductValues)
                                                        .HasForeignKey(x => x.ProductId)
                                                        .IsRequired();
            modelBuilder.Entity<Order>().HasOne(x => x.User)
                                        .WithMany(r => r.Orders)
                                        .HasForeignKey(x => x.UserId)
                                        .IsRequired();
            modelBuilder.Entity<Order>().HasOne(x => x.DiscountCode)
                                        .WithMany(r => r.Orders)
                                        .HasForeignKey(x => x.DiscountCodeId);
            modelBuilder.Entity<OrderDetail>().HasOne(x => x.Product)
                                              .WithMany(r => r.OrderDetails)
                                              .HasForeignKey(r => r.ProductId)
                                              .IsRequired();
            modelBuilder.Entity<OrderDetail>().HasOne(x => x.Order)
                                              .WithMany(r => r.OrderDetails)
                                              .HasForeignKey(x => x.OrderId)
                                              .IsRequired();
            modelBuilder.Entity<OrderDetail>().HasOne(x => x.Product)
                                              .WithMany(r => r.OrderDetails)
                                              .HasForeignKey(x => x.ProductId)
                                              .IsRequired();
            modelBuilder.Entity<OrderDetail>().HasOne(x => x.DiscountProduct)
                                              .WithMany(x => x.OrderDetails)
                                              .HasForeignKey(x => x.DiscountProductId);
            modelBuilder.Entity<Stock>().HasOne(x => x.Product)
                                        .WithOne(x => x.Stock)
                                        .HasForeignKey<Stock>(x => x.ProductId)
                                        .IsRequired();
            modelBuilder.Entity<DiscountProduct>().HasOne(x => x.Product)
                                                  .WithMany(x => x.DiscountProducts)
                                                  .HasForeignKey(x => x.ProductId)
                                                  .IsRequired();
            modelBuilder.Entity<AttributeValue>().HasOne(x => x.AttributeProduct)
                                                 .WithMany(x => x.AttributeValues)
                                                 .HasForeignKey(x => x.AttributeId)
                                                 .IsRequired();
            modelBuilder.Entity<CartDetail>().HasOne(x => x.Product)
                                             .WithMany(r => r.CartDetails)
                                             .HasForeignKey(x => x.ProductId);
            modelBuilder.Entity<CartDetail>().HasOne(x => x.Cart)
                                             .WithMany(r => r.CartDetails)
                                             .HasForeignKey(x => x.CartId);
        }

    }
}
