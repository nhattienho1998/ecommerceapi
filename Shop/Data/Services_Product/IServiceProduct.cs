﻿using Shop.Helpers.Params;
using Shop.ViewModels;
using Shop.ViewModels.AttributeValue;
using Shop.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Product
{
    public interface IServiceProduct
    {
        Task<Object>CreateParentProduct(ProductParentForCreate product);
        Task<Object> CreateChildProduct(ProductChildForCreate product);
        ValidationResultViewModel ValidationBeforeCreateProduct(ProductParentForCreate product);
        ValidationResultViewModel ValidationBeforeCreateChildProduct(ProductChildForCreate product);
        ValidationResultViewModel ValidationBeforeCreateAttributeValue(ProductChildForCreate product);
        Task<Object> GetProductsForClient(ProductParams productParams);
        Task<Object> GetProductsForAdmin(ProductParams productParams);
        Task<Object> GetDetailProductForClient(int id);
        Task<Object> GetDetailProductForAdmin(int id);
        Task<Object> GetTrending();
    }
}
