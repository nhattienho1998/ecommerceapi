﻿using Microsoft.EntityFrameworkCore;
using Shop.Data.GoogleCloudStorage;
using Shop.Helpers;
using Shop.Helpers.Params;
using Shop.Models;
using Shop.ViewModels;
using Shop.ViewModels.AttributeValue;
using Shop.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Services_Product
{
    public class ServiceProduct : IServiceProduct
    {
        private readonly DataContext _context;
        private readonly ICloudStorage _cloudStorage;

        public ServiceProduct(DataContext context, ICloudStorage cloudStorage)
        {
            _context = context;
            _cloudStorage = cloudStorage;
        }

        public async Task<object> CreateChildProduct(ProductChildForCreate product)
        {
            var parent = _context.Products.FirstOrDefault(x => x.ProductId == product.ParentId);
            string name = parent.Name;
            foreach (var attribute in product.ListAtrribute)
            {
                name = name + " - " + attribute.Value;
            }
            var strPoster = await _cloudStorage.UploadFileAsync(product.Poster, parent.Link);
            var transaction = _context.Database.BeginTransaction();
            try
            {
                var newChildProduct = new Product
                {
                    ParentId = product.ParentId,
                    Price = product.Price,
                    Name = name,
                    BrandId = parent.BrandId,
                    CategoryId = parent.BrandId,
                    Poster = strPoster,
                    SKU = product.SKU,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    Status = "Actived"
                };
                await _context.Products.AddAsync(newChildProduct);
                await _context.SaveChangesAsync();
                foreach (var item in product.ListAtrribute)
                {
                    var newAttributeValue = new AttributeProductValue
                    {
                        AttributeId = item.AttributeId,
                        ProductId = newChildProduct.ProductId,
                        Value = item.Value,
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        Status = "Actived"
                    };
                    _context.AttributeProductValues.Add(newAttributeValue);
                    _context.SaveChanges();
                }

                var listAttributeValue = _context.AttributeProductValues.Where(x => x.ProductId == newChildProduct.ProductId);
                var listAttributeProduct = _context.AttributeProduct.ToList();
                var listAttributeValueForView = new List<Object>();
                foreach (var item in listAttributeValue)
                {
                    var key = listAttributeProduct.FirstOrDefault(x => x.AttributeId == item.AttributeId).Name;
                    var attribute = new
                    {
                        AttributeValueId = item.AttibuteValueId,
                        AttributeName = key,
                        Value = item.Value
                    };
                    listAttributeValueForView.Add(attribute);
                }
                var newStock = new Stock
                {
                    ProductId = newChildProduct.ProductId,
                    Quantity = 0,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    Status = "Hết hàng"
                };
                await _context.Stocks.AddAsync(newStock);
                await _context.SaveChangesAsync();
                transaction.Commit();
                return new
                {
                    ProductId = newChildProduct.ProductId,
                    ParentId = newChildProduct.ParentId,
                    Price = newChildProduct.Price,
                    Name = newChildProduct.Name,
                    Poster = newChildProduct.Poster,
                    SKU = newChildProduct.SKU,
                    CreateDate = newChildProduct.CreateDate,
                    UpdateDate = newChildProduct.UpdateDate,
                    Status = newChildProduct.Status,
                    listAttribute = listAttributeValueForView
                };
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                Console.WriteLine("Error occurred.");
                return null;
            }
        }

        public async Task<object> CreateParentProduct(ProductParentForCreate product)
        {
            var category = _context.Categories.FirstOrDefault(x => x.CategoryId == product.CategoryId);
            var brand = _context.Brands.FirstOrDefault(x => x.BrandId == product.BrandId);
            var newProduct = new Product
            {
                Price = product.Price,
                Name = product.Name,
                CategoryId = product.CategoryId,
                Poster = await _cloudStorage.UploadFileAsync(product.Poster, product.Link),
                Description = product.Description,
                SKU = product.SKU,
                BrandId = product.BrandId,
                Unit = product.Unit,
                Origin = product.Origin,
                ShortDescription = product.ShortDescription,
                Link = product.Link,
                IsActive = product.IsActive,
                IsTrending = product.IsTrending,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Status = "Actived"
            };
            await _context.Products.AddAsync(newProduct);
            await _context.SaveChangesAsync();
            return new
            {
                ProductId = newProduct.ProductId,
                Price = newProduct.Price,
                Name = newProduct.Name,
                Poster = newProduct.Poster,
                Description = newProduct.Description,
                SKU = newProduct.SKU,
                Brand = new
                {
                    BrandId = brand.BrandId,
                    BrandName = brand.Name
                },
                Unit = newProduct.Unit,
                Origin = newProduct.Origin,
                ShortDescription = newProduct.ShortDescription,
                Link = newProduct.Link,
                CreateDate = newProduct.CreateDate,
                UpdateDate = newProduct.UpdateDate,
                Status = newProduct.Status,
                Category = new
                {
                    CategoryId = category.CategoryId,
                    CategoryName = category.Name,
                    Decription = category.Description
                },
                IsActive = newProduct.IsActive,
                IsTrending = newProduct.IsTrending
            };
        }


        public ValidationResultViewModel ValidationBeforeCreateProduct(ProductParentForCreate product)
        {
            var totalName = _context.Products.Count(x => x.Name == product.Name);
            var totalSKU = _context.Products.Count(x => x.SKU == product.SKU);
            IDictionary<string, string> Errors = new Dictionary<string, string>();
            if (totalName >= 1 || totalSKU >= 1)
            {
                if (totalName >= 1)
                {
                    Errors.Add("Name", "Name is duplicated!");
                }
                if (totalSKU >= 1)
                {
                    Errors.Add("SKU", "SKU is duplicated!");
                }
                return new ValidationResultViewModel
                {
                    IsValid = false,
                    Errors = Errors
                };
            }
            else
            {
                return new ValidationResultViewModel
                {
                    IsValid = true
                };
            }
        }

        public ValidationResultViewModel ValidationBeforeCreateAttributeValue(ProductChildForCreate product)
        {
            var parentId = product.ParentId;
            var listAttribute = product.ListAtrribute;
            var listProductOption = _context.Products.Where(x => x.ParentId == parentId);
            var listAttributeValue = _context.AttributeProductValues.ToList();
            IDictionary<string, string> Errors = new Dictionary<string, string>();
            foreach (var item in listProductOption)
            {
                foreach (var attribute in listAttribute)
                {
                    var total = listAttributeValue.Count(x => x.ProductId == item.ProductId
                    && x.AttributeId == attribute.AttributeId
                    && x.Value == attribute.Value);
                    if (total > 0)
                    {
                        {
                            Errors.Add("Attribute", "Attribute is duplicated!");
                        }
                        return new ValidationResultViewModel
                        {
                            IsValid = false,
                            Errors = Errors
                        };
                    }
                }
            }
            return new ValidationResultViewModel
            {
                IsValid = true
            };
        }

        public ValidationResultViewModel ValidationBeforeCreateChildProduct(ProductChildForCreate product)
        {
            var parentId = product.ParentId;
            var listAttribute = product.ListAtrribute;
            var listProductOption = _context.Products.Where(x => x.ParentId == parentId);
            var listAttributeValue = _context.AttributeProductValues.ToList();
            var totalSKU = _context.Products.Count(x => x.SKU == product.SKU);
            IDictionary<string, string> Errors = new Dictionary<string, string>();
            if (totalSKU >= 1)
            {
                Errors.Add("SKU", "SKU is duplicated!");
                return new ValidationResultViewModel
                {
                    IsValid = false,
                    Errors = Errors
                };
            }
            else
            {
                int count;
                foreach (var item in listProductOption)
                {
                    count = 0;
                    foreach (var attribute in listAttribute)
                    {
                        var total = listAttributeValue.Count(x => x.ProductId == item.ProductId
                        && x.AttributeId == attribute.AttributeId
                        && x.Value == attribute.Value);
                        if (total > 0)
                        {
                            count = count + 1;
                        }
                    }
                    if (count == listAttribute.Count())
                    {
                        Errors.Add("Attribute", "Attribute is duplicated!");
                        return new ValidationResultViewModel
                        {
                            IsValid = false,
                            Errors = Errors
                        };
                    }
                }
            }
            return new ValidationResultViewModel
            {
                IsValid = true
            };
        }

        public async Task<object> GetProductsForClient(ProductParams productParams)
        {
            var keyword = productParams.Keyword;
            var categoryId = productParams.Category;
            var brandId = productParams.Brand;
            var topPrice = productParams.TopPrice;
            var lastPrice = productParams.LastPrice;
            var listCategory = _context.Categories.Where(x => x.Status == "Actived").ToList();
            var listBrand = _context.Brands.Where(x => x.Status == "Actived").ToList();
            var listProduct = _context.Products.Where(x => (x.ParentId == null || x.ParentId == 0) && (x.IsActive == true)).AsQueryable();
           

            if (categoryId != null && categoryId > 0)
            {
                var cat = listCategory.FirstOrDefault(x => x.CategoryId == categoryId);
                if (cat.ParentId == null || cat.ParentId == 0)
                {
                    var listProductOfParent = listProduct.Where(x => x.CategoryId == categoryId);
                    var listcatchild = listCategory.Where(x => x.ParentId == categoryId);
                    var listProductTemp = new List<Product>();
                    if (listProductOfParent != null)
                    {
                        foreach (var item in listProductOfParent)
                        {
                            listProductTemp.Add(item);
                        }
                    }
                    foreach (var item in listcatchild)
                    {
                        var listTemp = listProduct.Where(x => x.CategoryId == item.CategoryId);
                        foreach (var productTemp in listTemp)
                        {
                            listProductTemp.Add(productTemp);
                        }
                    }
                    listProduct = listProductTemp.AsQueryable();

                }
                else
                {
                    listProduct = listProduct.Where(x => x.CategoryId == categoryId);
                }
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                listProduct = listProduct.Where(x => x.Name.ToLower().Contains(keyword.ToLower()));
            }
            if (brandId != null && brandId > 0)
            {
                listProduct = listProduct.Where(x => x.BrandId == brandId);
            }
            if (topPrice > 0 && lastPrice > 0)
            {
                listProduct = listProduct.Where(x => x.Price >= topPrice && x.Price <= lastPrice);
            }
            var totalItems = listProduct.Count();
            var totalPages = (int)Math.Ceiling((double)totalItems / (double)productParams.PageSize);
            var listProductForView = new List<Object>();
            foreach (var item in listProduct)
            {
                var brand = listBrand.FirstOrDefault(x => x.BrandId == item.BrandId);
                var category = listCategory.FirstOrDefault(x => x.CategoryId == item.CategoryId);
                var product = new
                {
                    ProductId = item.ProductId,
                    Price = item.Price,
                    Name = item.Name,
                    Poster = item.Poster,
                    SKU = item.SKU,
                    Brand = new
                    {
                        BrandId = brand.BrandId,
                        BrandName = brand.Name
                    },
                    Unit = item.Unit,
                    Origin = item.Origin,
                    Link = item.Link,
                    Category = new
                    {
                        CategoryId = category.CategoryId,
                        CategoryName = category.Name
                    }
                };
                listProductForView.Add(product);
            }
            var temp = listProductForView.AsQueryable();
            var listProducts = PagedList<Object>.Create(temp, productParams.PageNumber, productParams.PageSize);
            return new
            {
                listProducts = listProducts,
                totalPages = totalPages,
                totalItems = totalItems,
                pageNumber = productParams.PageNumber
            };
        }

        public async Task<object> GetProductsForAdmin(ProductParams productParams)
        {
            var keyword = productParams.Keyword;
            var categoryId = productParams.Category;
            var brandId = productParams.Brand;
            var topPrice = productParams.TopPrice;
            var lastPrice = productParams.LastPrice;
            var listCategory = _context.Categories.ToList();
            var listBrand = _context.Brands.ToList();
            var listProduct = _context.Products.Where(x => x.ParentId == null || x.ParentId == 0).AsQueryable();
            if (categoryId != null && categoryId > 0)
            {
                var cat = listCategory.FirstOrDefault(x => x.CategoryId == categoryId);
                if (cat.ParentId == null || cat.ParentId == 0)
                {
                    var listProductOfParent = listProduct.Where(x => x.CategoryId == categoryId);
                    var listcatchild = listCategory.Where(x => x.ParentId == categoryId);
                    var listProductTemp = new List<Product>();
                    if (listProductOfParent != null)
                    {
                        foreach (var item in listProductOfParent)
                        {
                            listProductTemp.Add(item);
                        }
                    }
                    foreach (var item in listcatchild)
                    {
                        var listTemp = listProduct.Where(x => x.CategoryId == item.CategoryId);
                        foreach (var productTemp in listTemp)
                        {
                            listProductTemp.Add(productTemp);
                        }
                    }
                    listProduct = listProductTemp.AsQueryable();

                }
                else
                {
                    listProduct = listProduct.Where(x => x.CategoryId == categoryId);
                }
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                listProduct = listProduct.Where(x => x.Name.ToLower().Contains(keyword));
            }
            if (brandId != null && brandId > 0)
            {
                listProduct = listProduct.Where(x => x.BrandId == brandId);
            }
            if (topPrice > 0 && lastPrice > 0)
            {
                listProduct = listProduct.Where(x => x.Price >= topPrice && x.Price <= lastPrice);
            }
            var totalItems = listProduct.Count();
            var totalPages = (int)Math.Ceiling((double)totalItems / (double)productParams.PageSize);
            var listProductForView = new List<Object>();
            foreach (var item in listProduct)
            {
                var brand = listBrand.FirstOrDefault(x => x.BrandId == item.BrandId);
                var category = listCategory.FirstOrDefault(x => x.CategoryId == item.CategoryId);
                var product = new
                {
                    ProductId = item.ProductId,
                    Price = item.Price,
                    Name = item.Name,
                    Poster = item.Poster,
                    Description = item.Description,
                    SKU = item.SKU,
                    Brand = new
                    {
                        BrandId = brand.BrandId,
                        BrandName = brand.Name
                    },
                    Unit = item.Unit,
                    Origin = item.Origin,
                    ShortDescription = item.ShortDescription,
                    Link = item.Link,
                    Category = new
                    {
                        CategoryId = category.CategoryId,
                        CategoryName = category.Name,
                        Decription = category.Description
                    },
                    IsActive = item.IsActive,
                    IsTrending = item.IsTrending
                };
                listProductForView.Add(product);
            }
            var temp = listProductForView.AsQueryable();
            var listProducts = PagedList<Object>.Create(temp, productParams.PageNumber, productParams.PageSize);
            return new
            {
                listProducts = listProducts,
                totalPages = totalPages,
                totalItems = totalItems,
                pageNumber = productParams.PageNumber
            };
        }

        public async Task<object> GetDetailProductForClient(int id)
        {
            var product = await _context.Products.FirstOrDefaultAsync(x => x.ProductId == id);
            var brand = await _context.Brands.FirstOrDefaultAsync(x => x.BrandId == product.BrandId);
            var category = await _context.Categories.FirstOrDefaultAsync(x => x.CategoryId == product.CategoryId);
            var parentCategory = await _context.Categories.FirstOrDefaultAsync(x => x.CategoryId == category.ParentId);
            var listOptionProduct = _context.Products.Where(x => x.ParentId == id).ToList();
            var listAttributeValue = _context.AttributeProductValues.ToList();
            var listAttribute = _context.AttributeProduct.ToList();
            var listBreadcrumbs = new List<CategoryForBreadcrumbs>();
            var listImages = _context.ImageProducts.ToList();
            var listStocks = _context.Stocks.ToList();
            var brandToView = new
            {
                BrandId = brand.BrandId,
                Name = brand.Name,
                Link = brand.Link
            };
            var categoryForView = new
            {
                CategoryId = category.CategoryId,
                Name = category.Name,
                Poster = category.Poster,
                ParentId = category.ParentId,
                Description = category.Description,
                Link = category.Link
            };
            listBreadcrumbs.Add(new CategoryForBreadcrumbs
            {
                CategoryId = category.CategoryId,
                Name = category.Name,
                Poster = category.Poster,
                ParentId = category.ParentId,
                Description = category.Description,
                Link = category.Link,
                Level = 2
            });
            if (parentCategory != null)
            {
                var parentBreadCrumb = new CategoryForBreadcrumbs
                {
                    CategoryId = parentCategory.CategoryId,
                    Name = parentCategory.Name,
                    Poster = parentCategory.Poster,
                    ParentId = parentCategory.ParentId,
                    Description = parentCategory.Description,
                    Link = parentCategory.Link,
                    Level = 1
                };
                listBreadcrumbs.Add(parentBreadCrumb);
            }
            listBreadcrumbs.OrderBy(x => x.Level);
            var listOptionProductForView = new List<object>();
            var listAttributeOfProduct = new List<object>();
            var listValue = new List<AttributeProductValue>();
            foreach (var productOption in listOptionProduct)
            {
                var listTemp = new List<object>();
                var listImageOfProduct = listImages.Where(x => x.ProductId == productOption.ProductId);
                foreach (var item in listImageOfProduct)
                {
                    var image = new
                    {
                        Id = item.ImageId,
                        Url = item.Url,
                        DateAdded = item.DateAdded
                    };
                    listTemp.Add(image);
                }
                var listAttributeOfProductTemp = listAttributeValue.Where(x => x.ProductId == productOption.ProductId);
                var listAttributeValueOfProduct = new List<string>();
                foreach (var item in listAttributeOfProductTemp)
                {
                    //var attributeName = listAttribute.FirstOrDefault(x => x.AttributeId == item.AttributeId).Name;
                    listAttributeValueOfProduct.Add(item.Value);
                    listValue.Add(item);
                }
                string stockStatus = "";
                var proStock = listStocks.FirstOrDefault(x => x.ProductId == productOption.ProductId);
                if(proStock != null)
                {
                    stockStatus = proStock.Status;
                }
                var option = new
                {
                    Id = productOption.ProductId,
                    Name = productOption.Name,
                    Price = productOption.Price,
                    SKU = productOption.SKU,
                    ListAttributeValue = listAttributeValueOfProduct,
                    ListImages = listTemp,
                    stockStatus = stockStatus
                };
                listOptionProductForView.Add(option);
            }
            var listIdAttribute = listValue.Select(x => x.AttributeId).Distinct().ToList();
            foreach (var a in listIdAttribute)
            {
                var atribute = listAttribute.FirstOrDefault(x => x.AttributeId == a);
                var listItem = listAttributeValue.Where(x => x.AttributeId == a).Select(x => x.Value).Distinct().ToList();
                var result = new
                {
                    AttributeId = atribute.AttributeId,
                    Name = atribute.Name,
                    listItems = listItem
                };
                listAttributeOfProduct.Add(result);
            }

            return new
            {
                ProductId = product.ProductId,
                Name = product.Name,
                Category = categoryForView,
                Poster = product.Poster,
                Description = product.Description,
                SKU = product.SKU,
                Brand = brandToView,
                Unit = product.Unit,
                Origin = product.Origin,
                ShortDescription = product.ShortDescription,
                Link = product.Link,
                IsActive = product.IsActive,
                IsTrending = product.IsTrending,
                ListProductOptions = listOptionProductForView,
                ListAttribute = listAttributeOfProduct,
                ListBreadcrumbs = listBreadcrumbs
            };
        }

        public async Task<object> GetDetailProductForAdmin(int id)
        {
            var product = await _context.Products.FirstOrDefaultAsync(x => x.ProductId == id);
            var brand = await _context.Brands.FirstOrDefaultAsync(x => x.BrandId == product.BrandId);
            var category = await _context.Categories.FirstOrDefaultAsync(x => x.CategoryId == product.CategoryId);
            var listOptionProduct = _context.Products.Where(x => x.ParentId == id).ToList();
            var listAttributeValue = _context.AttributeProductValues.ToList();
            var listAttribute = _context.AttributeProduct.ToList();
            var listImages = _context.ImageProducts.ToList();
            var brandToView = new
            {
                BrandId = brand.BrandId,
                Name = brand.Name,
                Link = brand.Link
            };
            var categoryForView = new
            {
                CategoryId = category.CategoryId,
                Name = category.Name,
                Poster = category.Poster,
                ParentId = category.ParentId,
                Description = category.Description,
                Link = category.Link
            };
            var listOptionProductForView = new List<object>();
            var listAttributeOfProduct = new List<object>();
            var listValue = new List<AttributeProductValue>();
            foreach (var productOption in listOptionProduct)
            {
                var listTemp = new List<object>();
                var listImageOfProduct = listImages.Where(x => x.ProductId == productOption.ProductId);
                foreach (var item in listImageOfProduct)
                {
                    var image = new
                    {
                        Id = item.ImageId,
                        Url = item.Url,
                        DateAdded = item.DateAdded
                    };
                    listTemp.Add(image);
                }
                var listAttributeOfProductTemp = listAttributeValue.Where(x => x.ProductId == productOption.ProductId);
                var listAttributeValueOfProduct = new List<object>();
                foreach (var item in listAttributeOfProductTemp)
                {
                    var attributeName = listAttribute.FirstOrDefault(x => x.AttributeId == item.AttributeId).Name;
                    var attibuteValue = new
                    {
                        AttributeValueId = item.AttibuteValueId,
                        AttributeId = item.AttributeId,
                        AttributeName = attributeName,
                        Value = item.Value
                    };
                    listAttributeValueOfProduct.Add(attibuteValue);
                    listValue.Add(item);
                }
                var option = new
                {
                    Id = productOption.ProductId,
                    Name = productOption.Name,
                    Price = productOption.Price,
                    SKU = productOption.SKU,
                    ListAttributeValue = listAttributeValueOfProduct,
                    ListImages = listTemp
                };
                listOptionProductForView.Add(option);
            }
            var listIdAttribute = listValue.Select(x => x.AttributeId).Distinct().ToList();
            foreach (var a in listIdAttribute)
            {
                var atribute = listAttribute.FirstOrDefault(x => x.AttributeId == a);
                var listItem = listAttributeValue.Where(x => x.AttributeId == a).Select(x => x.Value).Distinct().ToList();
                var result = new
                {
                    AttributeId = atribute.AttributeId,
                    Name = atribute.Name,
                    listItems = listItem
                };
                listAttributeOfProduct.Add(result);
            }

            return new
            {
                ProductId = product.ProductId,
                Name = product.Name,
                Category = categoryForView,
                Poster = product.Poster,
                Description = product.Description,
                SKU = product.SKU,
                Brand = brandToView,
                Unit = product.Unit,
                Origin = product.Origin,
                ShortDescription = product.ShortDescription,
                Link = product.Link,
                IsActive = product.IsActive,
                IsTrending = product.IsTrending,
                ListProductOptions = listOptionProductForView,
                ListAttribute = listAttributeOfProduct
            };
        }

        public async Task<object> GetTrending()
        {
            var listCategory = _context.Categories.Where(x => x.Status == "Actived").ToList();
            var listBrand = _context.Brands.Where(x => x.Status == "Actived").ToList();
            var listTreding = _context.Products.Where(x => (x.ParentId == 0 || x.ParentId == null) && (x.IsTrending == true && x.IsActive == true)).Take(8);
            var result = new List<object>();
            foreach (var item in listTreding)
            {
                var brand = listBrand.FirstOrDefault(x => x.BrandId == item.BrandId);
                var category = listCategory.FirstOrDefault(x => x.CategoryId == item.CategoryId);
                var product = new
                {
                    ProductId = item.ProductId,
                    Price = item.Price,
                    Name = item.Name,
                    Poster = item.Poster,
                    SKU = item.SKU,
                    Brand = new
                    {
                        BrandId = brand.BrandId,
                        BrandName = brand.Name
                    },
                    Unit = item.Unit,
                    Origin = item.Origin,
                    Link = item.Link,
                    Category = new
                    {
                        CategoryId = category.CategoryId,
                        CategoryName = category.Name
                    }
                };
                result.Add(product);
            }
            return result;
        }

    }
}
