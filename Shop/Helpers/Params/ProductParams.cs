﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Helpers.Params
{
    public class ProductParams
    {
        private const int MaxPageSize = 50;
        public int PageNumber { get; set; } = 1;
        private int pageSize = 12;
        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = (value > MaxPageSize) ? MaxPageSize : value; }
        }
        public string Keyword { get; set; }
        public int Category { get; set; }
        public int Brand { get; set; }
        public decimal TopPrice { get; set; }
        public decimal LastPrice { get; set; }
    }
}
